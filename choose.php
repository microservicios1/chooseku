<html lang="es">
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Solicitudes</title>
    <link rel="stylesheet" type="text/css" href="StRod.css">
    <style>
      #FR,#Sol,#Ger,#Dir,#Req,#ASol,#AFin,#Pro,#Apl,#SVM,#SVCPU,#SRAM,#SD,#DP,#PP,#Com
      {
        width:120px;
        float:right;
        margin-right:40px;
      }
      .s1
      {
        float:right;
        margin-right:80px;
        display:inline;
        text-shadow: 1px 1px 5px #FFFFFF;
      }
      .s2
      {
        display:inline;
        float:right;
        margin-right:200px;
        text-shadow: 1px 1px 5px #FFFFFF;
      }
      .container2
      {
        padding: 6px ;
        display: inline-block;
        float:right;
      }
    </style>
    <?php
      include 'dbc.php';
      include 'session.php';
      $conn = mysqli_connect($host,$user,$pass,$db);
      if($_POST['sola']=="")
        header('Location: '.$sols);
      $vm=array('cpu' =>0,'mem' =>0,'sd'=>0,'sdq'=>0);
      $sql="select CPU,Mem,SD,SDQ from filtmachine where No=".$_POST['sola'];
      $re = mysqli_query($conn,$sql);
      if(!$re)
        echo "Conexion con BD fallida";
      else
        {
          $vmpeople=0;
          while($row0 = mysqli_fetch_array($re))
           {
             $vmpeople++;
             $vm['cpu'] += $row0['CPU'];
             $vm['mem'] += $row0['Mem'];
             $vm['sd'] += $row0['SD'];
             $vm['sdq'] += $row0['SDQ'];
           }
        }
      $sql="select * from filtro where FR2=".$_POST['sola'];
      $re = mysqli_query($conn,$sql);
      if(!$re)
        echo "Conexion con BD fallida";
      else
          $thatData = mysqli_fetch_array($re);
    ?>
  </head>
  <body>
    <div class="container" >
    <ul id="nav">
      <li><a href="<?php echo $lgout;?>">Cerrar sesion</a></li>
      <li>User : <?php echo $_COOKIE['myname'];?></li>
      <li><a href="<?php echo $inside;?>">Menu</a></li>
      <li clas="current"><a href="">Solicitudes</a></li>
    </ul>
    <br>
    <form method='post' id="thatform" action='judement.php' >
      <div class="container2" >
        <input type="submit" value="Aprobar Proyecto" name="thisisnice" id="thisisnice" >
        <input type="submit" value="Rechazar Proyecto" name="mercifullno" id="mercifullno">
        <input type="button" value="Rechazar y eliminar"  onclick="why()">
      </div>
      <br><br><br>
      <div class="lineR">
        Fecha de recepcion : <input type="text" id="FR" name="FR"  value="<?php echo $thatData['FR']; ?>" readonly="readonly">
      </div>
      <div class="lineR">
        Solicitante : <input type="text" id="Sol" name="Sol" value="<?php echo $thatData['Sol']; ?>" readonly="readonly">
        <input type="hidden"  id="sola" name="sola" value="<?php echo $_POST['sola']; ?>" >
      </div>
      <div class="lineR">
        Tipo de requerimiento : <input type="text" id="Req" name="Req" value="<?php echo $thatData['Req']; ?>" readonly="readonly">
      </div>
      <br><br>
      <div class="lineR">
        Gerencia : <input type="text" id="Ger" name="Ger" value="<?php echo $thatData['Ger']; ?>" readonly="readonly">
      </div>
      <div class="lineR">
        Direccion : <input type="text" id="Dir" name="Dir" value="<?php echo $thatData['Dir']; ?>" readonly="readonly">
      </div>
      <div class="lineR">
        Proyecto : <input type="text" id="Pro" name="Pro" value="<?php echo $thatData['Pro']; ?>" readonly="readonly">
      </div>
      <br><br>
      <div class="lineR">
        Ambiente Entregado: <select name="AFin" id="AFin" >
          <option <?php if($thatData['AFin'] == ''){echo("selected");}?> value=""></option>
          <?php
            $re = mysqli_query($conn,"select nombre from ambiente");
            if(! $re)
              echo "<option value=\"Pendiente\">Pendiente</option> ";
            else
              while($row = mysqli_fetch_array($re))
              {
                $o ="<option ";
                if($thatData['AFin'] == $row['nombre'])
                  $o.=" selected ";
                $o.="value=\"".$row['nombre']."\">".$row['nombre']."</option>";
                echo $o;
              }
          ?>
       </select>
      </div>
      <div class="lineR">
        <?php
          $amb=array('Desarrollo','QA','Produccion','Test','Pendiente','Pruebas','Otro','POC');
          $sol=explode("-",$thatData['ASol']);
          $Solici="";
          for($i=0;$i<8;$i++)
          {
            if($sol[$i]=="1" && $Solici!="")
              $Solici .=", ".$amb[$i];
            if($sol[$i]=="1" && $Solici=="")
              $Solici .=$amb[$i];
          }
        ?>
        Ambiente Solicitado : <input type="text" id="ASol" name="ASol" value="<?php echo $Solici ?>" readonly="readonly">
      </div>
      <div class="lineR">
        Aplicacion : <input type="text" id="Apl" name="Apl" value="<?php echo $thatData['Apl']; ?>" readonly="readonly">
      </div>
      <br><br>
      <div class="lineR">
        Descripcion de proyecto : <input type="text" id="DP" name="DP" value="<?php echo $thatData['DP']; ?>" readonly="readonly">
      </div>
      <div class="lineR">
        Proveedor de proyecto : <input type="text" id="PP" name="PP" value="<?php echo $thatData['PP']; ?>" readonly="readonly">
      </div>
      <div class="lineR">
        Comentarios : <input type="text" id="Com" name="Com" value="<?php echo $thatData['Com']; ?>" readonly="readonly">
      </div>
      <br><br>
      <div style="margin-left:40px; text-shadow: 1px 1px 5px #151f6b;"><br>
        <div class="lineR">Tiempo implementaci&oacute;n :<div class="s1"><?php  for($i=0;$i<20;$i++) echo "&nbsp;"; echo $thatData['TEI'];?></div></div>
        <div class="lineR">Total VMs :<div class="s2"><?php  for($i=0;$i<24;$i++) echo "&nbsp;"; echo $vmpeople;?></div></div>
        <br>
        <div class="lineR">Horario de operacion :<div class="s1"><?php  for($i=0;$i<22;$i++) echo "&nbsp;"; echo $thatData['HO'];?></div></div>
        <div class="lineR">Total vCPU :<div class="s2"><?php  for($i=0;$i<21;$i++) echo "&nbsp;"; echo $vm['cpu'];?></div></div>
        <br>
        <div class="lineR">No. Usuarios :<div class="s1"><?php  for($i=0;$i<35;$i++) echo "&nbsp;"; echo $thatData['NU'];?></div></div>
        <div class="lineR">Total RAM :<div class="s2"><?php  for($i=0;$i<22;$i++) echo "&nbsp;"; echo $vm['mem'];?></div></div>
        <br>
        <input type="hidden" id="thatbox" name="thatbox" value="">
        <div class="lineR">Ingresos :<div class="s1"><?php  for($i=0;$i<42;$i++) echo "&nbsp;"; echo $thatData['Ing'];?></div></div>
        <div class="lineR">Total Disco :<div class="s2"><?php  for($i=0;$i<22;$i++) echo "&nbsp;"; echo $vm['sd'];?></div></div>
        <br>
        <div class="lineR">Disponibilidad Requerida :<div class="s1"><?php  for($i=0;$i<14;$i++) echo "&nbsp;"; echo $thatData['DR'];?></div></div>
        <br><br>
        <?php
          echo "<script>var solic=".$_POST['sola'].";</script>";
          echo "<input type=\"button\" style=\"margin-top:10px;diplay:inline;\" onclick=\"window.open('".$machk."?no='+solic+'&acc=2','','menubar=0,titlebar=0,width=800,height=400,resizable=0,left=40px,top=250px')\" value=\"Mostrar Maquinas Virtuales\" />";
          echo "<script>var some=".$_POST['sola'].";</script>";
        ?>
        <input type="button" style="margin-left:40px;display:inline; " id="Dia" onclick="window.open('cleannames.php?no='+some+'&act=1','','menubar=0,titlebar=0,width=650,height=580,resizable=0,left=40px,top=50px')" value="Mostrar Diagrama Infraestructura" />
        <br>
      </div>
    </form>
    <button type="button"  class="evilbtn">Tecnologias Cloud</button>
    <br><br><br><br>
    </div>
  </body>
  <script>
    function why()
    {
      var person = prompt("Motivo de rechazo:", "");
        if (person != null&&person != "")
        {
          document.getElementById("thatbox").value = person;
          document.getElementById('thatform').submit();
        }
        else
          alert("Es necesario especificar motivo de rechazo");
    }
</script>
</html>